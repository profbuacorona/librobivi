package bivi;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * 
 * <p>
 * E' un libro a bivi.
 * Si crea specificando il titolo: <br />  
 * 
 * <code>
 * 		Book book = new Book("Il mistero del bla bla");
 * </code>
 * </p>
 *  <p>
 *  Le pagine vengono aggiunte.<br /> 
 *  <code>
 * 		book.add(page1);<br /> 
 * 		book.add(page2);
 * </code>
 * </p>
 * @author Gianni Bua Corona
 *
 */


public class Book {

	private static final Icon NEXT_PAGE = new ImageIcon("img/next.png");
	//private static final Icon PREVIOUS_PAGE = new ImageIcon("img/previous.png");
	
	private JButton nextButton;
	
	private JFrame frame;
	private CardLayout layout;
	private JPanel panel;
	
	private Vector<Page> pages;

	private boolean first;
	
	/**
	 * 
	 * @param title il titolo del libro
	 */
	public Book(String title) {
		
		first = true;
		
		pages = new Vector<>();

		frame = new JFrame("Libro a bivi: " + title);
		layout = new CardLayout();
		panel = new JPanel(layout);
		frame.getContentPane().add(panel);
		
		nextButton = new JButton(NEXT_PAGE);
		frame.getContentPane().add(nextButton, BorderLayout.EAST);
		
		nextButton.addActionListener(new ActionListener() {
			
			private int index; //indice della pagina corrente

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 layout.next(panel);
				 index++;
				 
				 Page page = pages.get(index);
				
				 if(page.isLast() || page.hasBivi()) nextButton.setVisible(false);
			
				
			}
		});
		
		
		
		
		frame.setBounds(100, 100, 700, 500);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

	/**
	 * 
	 * @param page la pagina da aggiungere
	 */
	public void add(Page page) {

		pages.add(page);

		if(first) {

			first = false;	
			if(page.isLast() || page.hasBivi() ) {
				nextButton.setVisible(false);
			} else nextButton.setVisible(true);
		}

		panel.add(page.getPanel(), page.toString());

		Option[] options = page.getOptions();

		for (int i = 0; i < options.length; i++) {

			Option option = options[i];

			if(option == null) continue;
			JButton button = option.getButton();

			button.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub

					Page page2 = option.getLink();
					layout.show(panel, page2.toString());
					
					if(page2.isLast() || page2.hasBivi() ) {
						nextButton.setVisible(false);
					} else nextButton.setVisible(true);
				}
			});

		}

	}
	
}
