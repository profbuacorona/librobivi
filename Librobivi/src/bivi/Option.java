package bivi;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
/**
 * Una option � un oggetto che permette di realizzare i bivi della storia.
 * Una option ha un testo che descrive un evento del racconto. 
 * Una option pu� essere selezionata dal lettore, in questo l'evento descritto dalla option si verifica nella storia..
 * Quando una option viene selezionata si visualizza la relativa page, cio� la option si comporta come un link ipertestuale.
 * @author Gianni Bua Corona
 *
 */
public class Option {

	private JButton button;
	private Page page; //link

	/**
	 * 
	 * @param string il testo della option che descrive un possibile evento del racconto
	 */
	public Option(String string) {
		// TODO Auto-generated constructor stub
		
		button = new JButton(string);
		
	}

	/**
	 * 
	 * @param page la pagia a cui il lettore deve saltare per continuare la lettura
	 */
	public void setLink(Page page) {
		
		this.page = page;
	}

	JButton getButton() {
		// TODO Auto-generated method stub
		
		return button;
	}
	
	Page getLink() {
		
		return page;
	}

}
