package bivi;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * Una pagina di un libro a bivi � composta da un testo, da una freccia di navigazione per passare 
 * alla pagina successiva e da un numero variabile di opzioni che il lettore puo' scegliere e che realizzano
 * il cosidetto bivio.
 * @author Gianni Bua Corona
 *
 */
public class Page {
	
	private JPanel mainPanel, southPanel;;
	
	private Option[] options;
	private int i;
	
	private boolean last;

	private boolean hasBivi;

	/**
	 * 
	 * @param text il testo della pagina
	 *
	 * @param last true <=> questa � l'ultima pagina
	 */
	public Page(String text, boolean last) {
		
		this.last = last;
		options = new Option[4];
		JLabel label = new JLabel(text);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setVerticalAlignment(SwingConstants.CENTER);
		mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(label);
		
		southPanel = new JPanel();
		mainPanel.add(southPanel, BorderLayout.SOUTH);
		
	}
	

	public Page(String text) {
		this(text, false);
		// TODO Auto-generated constructor stub
	}



	/**
	 * 
	 * @param option l'opzione da aggiungere
	 */
	public void add(Option option) {
		// TODO Auto-generated method stub
		
		hasBivi = true;
		JPanel panel = new JPanel();
		panel.add(option.getButton());
		
		southPanel.add(panel);
		
		options[i] = option;
		i++;
		
		
		
	}
	
	Option[] getOptions() {
		
		return options;
		
	}

	JPanel getPanel() {
		// TODO Auto-generated method stub
		return mainPanel;
	}


	public boolean isLast() {
		// TODO Auto-generated method stub
		return last;
	}


	boolean hasBivi() {
		// TODO Auto-generated method stub
		return hasBivi;
	}




}
