import bivi.Book;
import bivi.Option;
import bivi.Page;

public class BiviApp {
	
	private String text1 = "Bla bla bla ... ", 
			text2 = "... e vissero felici e contenti.", 
			text3 = "... ma questa � un'altra storia.",
			text4 = "... e uscimmo a veder le stelle.";

	public BiviApp() {
		super();
		// TODO Auto-generated constructor stub
		Book book = new Book("Il mistero del bla bla");
		Page page, page1, page2, page3, page4;
		page = new Page("pagina 1");
		page1 = new Page(text1);
		page2 = new Page(text2, true);
		page3 = new Page(text3, true);
		page4 = new Page(text4, true);
		
		
	

		Option option1, option2, option3;
		option1 = new Option("Bla perde il treno");
		option2 = new Option("Bla prende il treno sbagliato");
		option3 = new Option("Bla riesce a prendere il treno");
		page1.add(option1);
		page1.add(option2);
		page1.add(option3);
		option1.setLink(page2);
		option2.setLink(page3);
		option3.setLink(page4);
		
		
		//dopo le option per ilmproblema degli ascoltatori
		//book.add(page);
		
	//
		
		book.add(page);
		book.add(page1);
		book.add(page2);
		book.add(page3);
		book.add(page4);
		
		
	}
	
	public static void main(String[] args) {
		new BiviApp();
//		new BiviApp();
//		new BiviApp();
	}
	

}
